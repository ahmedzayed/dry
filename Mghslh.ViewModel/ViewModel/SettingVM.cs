﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mghslh.ViewModel.ViewModel
{
   public class SettingVM
    {
        public int Id { get; set; }
        public string Details { get; set; }
        public string Titel { get; set; }
        public string Image { get; set; }
    }
}
