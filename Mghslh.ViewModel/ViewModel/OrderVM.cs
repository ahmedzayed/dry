﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mghslh.ViewModel.ViewModel
{
   public class OrderVM
    {
        public int Id { get; set; }
        public decimal TotalPrice { get; set; }
        public string UserName { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderCase { get; set; }
        public string DryName { get; set; }
        public string StageName { get; set; }
    }
}
