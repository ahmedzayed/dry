﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mghslh.ViewModel.ViewModel
{
  public  class ServicesPriceFM
    {
        public int Id { get; set; }
        public int ServiceId { get; set; }
        public decimal Price { get; set; }
        public int DB_A40639_DryClean { get; set; }
        public int ServiceType { get; set; }
    }
}
