﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mghslh.ViewModel.ViewModel
{
  public  class MghslhVM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string LatId { get; set; }
        public string LangId { get; set; }
        public string WorkingTime { get; set; }
        public string Details { get; set; }
        public string Image { get; set; }
        public string ManageName { get; set; }
        public bool Cases { get; set; }
    }
}
