﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mghslh.ViewModel.ViewModel
{
   public class OrderFM
    {
        public int Id { get; set; }
        public decimal TotalPrice { get; set; }
        public string UserId { get; set; }
        public DateTime OrderDate { get; set; }
        public int OrderCase { get; set; }
        public int DryId { get; set; }
        public int StageId { get; set; }
    }
}
