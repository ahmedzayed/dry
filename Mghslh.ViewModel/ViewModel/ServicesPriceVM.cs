﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mghslh.ViewModel.ViewModel
{
   public class ServicesPriceVM
    {
        public int Id { get; set; }
        public string ServiceName { get; set; }
        public decimal Price { get; set; }
        public string DB_A40639_DryCleanName { get; set; }
        public string ServiceTypeName { get; set; }
    }
}
