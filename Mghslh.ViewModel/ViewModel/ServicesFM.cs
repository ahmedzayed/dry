﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mghslh.ViewModel.ViewModel
{
   public class ServicesFM
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Image { get; set; }
        public int ServicesType { get; set; }

    }
}
