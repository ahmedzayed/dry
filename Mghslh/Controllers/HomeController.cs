﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var User1 = User.Identity.GetUserName();
            if (User.IsInRole("Admin"))
            {
                return RedirectToAction("Index", "AdminHome", new { area = "Admin" });
            }
            else
            {
                return View();

            }
            
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        public ActionResult Header()
        {
            var model = "SP_GetAllHeadrs".ExecuParamsSqlOrStored(false).AsList<SettingVM>().FirstOrDefault();

            return PartialView(model);
        }

        public ActionResult AboutUs()
        {
            var model = "SP_GetAllAboutUs".ExecuParamsSqlOrStored(false).AsList<SettingVM>().LastOrDefault();
            return PartialView(model);
        }
        public ActionResult OurExcellence()
        {
            var model = "SP_GetAllOurExcellence".ExecuParamsSqlOrStored(false).AsList<SettingVM>().Take(3);
            return PartialView(model);
        }
        public ActionResult Mghslh()
        {
            var model = "SP_GetAllOurExcellence".ExecuParamsSqlOrStored(false).AsList<SettingVM>().Take(3);
            return PartialView(model);
        }

        public ActionResult Createdry()
        {
            MghslhFM obj = new MghslhFM();
            return View(obj);
        }

        [HttpPost]
        public ActionResult Createdry(MghslhFM model)
        {
            var userid = User.Identity.GetUserId();

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath =Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.Image = "Uploads/Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }
            var data = "SP_AddMghslh".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name),
                "Details".KVP(model.Details),
                "LangId".KVP(model.LangId),
                "LatId".KVP(model.LatId),
                "ManageId".KVP(userid),
                "PhoneNumber".KVP(model.PhoneNumber),
                "WorkingTime".KVP(model.WorkingTime),
                "Cases".KVP(true),
                "Image".KVP(model.Image), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {
                TempData["success"] = "<script>alert(' تم الاضافه بنجاح');</script>";

            return    RedirectToAction("Index");


            }
            else
                TempData["success"] = "<script>alert(' لم يتم الاضافه ');</script>";

           return  RedirectToAction("Index");
        }

        public ActionResult Advertisement()
        {
            var model = "SP_GetAllAdvertisement".ExecuParamsSqlOrStored(false).AsList<AdvertisementVM>();

            return PartialView(model);
        }
    }
}