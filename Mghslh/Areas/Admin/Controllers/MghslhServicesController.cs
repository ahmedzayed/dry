﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Areas.Admin.Controllers
{
    public class MghslhServicesController : Controller
    {
        // GET: Admin/MghslhServices
        public ActionResult Index1()
        {




            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllMghslhServices".ExecuParamsSqlOrStored(false).AsList<PriceServicesVM>();
            return View(model);



        }


        public ActionResult Create()
        {
            PriceServicesFM Obj = new PriceServicesFM();

            return View(Obj);
        }
        //[HttpPost]
        //public JsonResult Create(PriceServicesFM model)
        //{

          
        //    var data = "SP_AddMghslhServices".ExecuParamsSqlOrStored(false, "Details".KVP(model.Details), "Titel".KVP(model.Titel), "Image".KVP(model.Image), "Id".KVP(model.Id)).AsNonQuery();
        //    if (data != 0)
        //    {


        //        return Json("success," + "تم الحفظ", JsonRequestBehavior.AllowGet);


        //    }
        //    else

        //        return Json("error," + "لم يتم الحفظ", JsonRequestBehavior.AllowGet);

        //}


        //public ActionResult Edit(int id = 0)
        //{

        //    var model = "SP_SelectMghslhServicesById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<PriceServicesVM>();
        //    if (model == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View("Create", model.FirstOrDefault());
        //}



        [HttpGet]
        public ActionResult Delete(int id)
        {
            PriceServicesFM obj = new PriceServicesFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/MghslhServices/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteMghslhServices".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}