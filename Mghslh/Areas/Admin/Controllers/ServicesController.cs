﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Areas.Admin.Controllers
{
    [Authorize]

    public class ServicesController : Controller
    {
        // GET: Admin/Services
        public ActionResult Index1()
        {




            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllServices".ExecuParamsSqlOrStored(false).AsList<ServicesVM>();
            return View(model);

        }


        public ActionResult Create()
        {

            ServicesFM Obj = new ServicesFM();
            var Servicetype = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Servicetype.AddRange("Sp_ServicesTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ServicetypeList = Servicetype;


            return View(Obj);
        }
        [HttpPost]
        public JsonResult Create(ServicesFM model)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.Image = "Uploads/Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }
            var data = "SP_AddServices".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name),"Image".KVP(model.Image) , "ServicesType".KVP(model.ServicesType),"Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                return Json("success," + "تم الحفظ", JsonRequestBehavior.AllowGet);


            }
            else

                return Json("error," + "لم يتم الحفظ", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectServicesById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<ServicesVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            ServicesVM obj = new ServicesVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Services/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteServices".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}