﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Areas.Admin.Controllers
{
    [Authorize]

    public class ServicesTypeController : Controller
    {
        // GET: Admin/ServicesType
        public ActionResult Index1()
        {
            
            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllServicesType".ExecuParamsSqlOrStored(false).AsList<ServicesTypeVM>();
            return View(model);

        }


        public ActionResult Create()
        {
            ServicesTypeVM Obj = new ServicesTypeVM();

            return View(Obj);
        }
        [HttpPost]
        public JsonResult Create(ServicesTypeVM model)
        {

            var data = "SP_AddServicesType".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name),"Details".KVP(model.Details), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                return Json("success," + "تم الحفظ", JsonRequestBehavior.AllowGet);


            }
            else

                return Json("error," + "لم يتم الحفظ", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectServicesTypeById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<ServicesTypeVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            ServicesTypeVM obj = new ServicesTypeVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/ServicesType/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteServicesType".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}