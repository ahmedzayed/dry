﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Areas.Admin.Controllers
{
    [Authorize]

    public class MghslhController : Controller
    {
        // GET: Admin/Mghslh
        public ActionResult Index1()
        {




            return View();
        }

        public ActionResult Search()
        {
            string UserId = User.Identity.GetUserId();
            var model = "Sp_GetAllDry".ExecuParamsSqlOrStored(false, "UserId".KVP(UserId)).AsList<MghslhVM>();
            return View(model);

        }


        public ActionResult Create()
        {
            MghslhVM Obj = new MghslhVM();

            return View(Obj);
        }
        [HttpPost]
        public JsonResult Create(MghslhFM model)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.Image = "Uploads/Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }
            string UserId = User.Identity.GetUserId();
            var data = "SP_AddDry".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name),
                "Details".KVP(model.Details),
                "LangId".KVP(model.LangId),
                "LatId".KVP(model.LatId),
                "ManageId".KVP(UserId),
                "PhoneNumber".KVP(model.PhoneNumber),
                "WorkingTime".KVP(model.WorkingTime),
                "Image".KVP(model.Image), "Case".KVP(false), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                return Json("success," + "تم الحفظ", JsonRequestBehavior.AllowGet);


            }
            else

                return Json("error," + "لم يتم الحفظ", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectMghslhById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<MghslhFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            MghslhFM obj = new MghslhFM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Mghslh/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteDry".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}