﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Areas.Admin.Controllers
{
    [Authorize]

    public class HeadrsController : Controller
    {
        // GET: Admin/Headrs
        public ActionResult Index1()
        {




            return View();
        }

        public ActionResult Search()
        {

            var model = "SP_GetAllHeadrs".ExecuParamsSqlOrStored(false).AsList<SettingVM>();
            return View(model);

        }


        public ActionResult Create()
        {
            SettingVM Obj = new SettingVM();

            return View(Obj);
        }
        [HttpPost]
        public JsonResult Create(SettingVM model)
        {

            if (Request.Files.Count > 0)
            {
                var file = Request.Files[0];

                if (file != null && file.ContentLength > 0)
                {
                    var fileNameWithExtension = Path.GetFileName(file.FileName);
                    var fileNameOnly = Path.GetFileNameWithoutExtension(file.FileName);
                    string RootPath = Server.MapPath("~/Uploads/Images/");
                    if (!Directory.Exists(RootPath))
                    {
                        Directory.CreateDirectory(RootPath);
                    }
                    var unique = new Guid();
                    var path = Path.Combine(RootPath, fileNameOnly + unique + Path.GetExtension(file.FileName));
                    file.SaveAs(path);
                    model.Image = "Uploads/Images/" + fileNameOnly + unique + Path.GetExtension(file.FileName);

                }



            }
            var data = "SP_AddHeadrs".ExecuParamsSqlOrStored(false, "Details".KVP(model.Details), "Titel".KVP(model.Titel), "Image".KVP(model.Image), "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                return Json("success," + "تم الحفظ", JsonRequestBehavior.AllowGet);


            }
            else

                return Json("error," + "لم يتم الحفظ", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectHeadrsById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<SettingVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            SettingVM obj = new SettingVM();
            obj.Id = id;
         
            return PartialView("~/Areas/Admin/Views/Headrs/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteHeadrs".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}