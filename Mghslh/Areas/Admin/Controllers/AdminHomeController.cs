﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Areas.Admin.Controllers
{
    public class AdminHomeController : Controller
    {
        // GET: Admin/Home
        [Authorize]
        public ActionResult Index()
        {
            var userid = User.Identity.GetUserId();
            var model = "SP_GetDrByUserId".ExecuParamsSqlOrStored(false,"UserId".KVP(userid)).AsList<MghslhFM>().FirstOrDefault();

            HttpCookie cookieDry = new HttpCookie("DrryId", "0");
            cookieDry.Value = model.Id.ToString();
            Response.SetCookie(cookieDry);
            Response.Cookies.Add(cookieDry);
            cookieDry.Expires.AddDays(10);
            var DrryId = Request.Cookies["DrryId"].Value;

            return View();
        }
    }
}