﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Areas.Admin.Controllers
{
    public class OrderController : Controller
    {
        // GET: Admin/Order
        public ActionResult Index1()
        {
            
            return View();
        }

        public ActionResult Search()
        {
            int DryId= Convert.ToInt32(Request.Cookies["DrryId"].Value);
            var model = "SP_GetAllOrderByDryId".ExecuParamsSqlOrStored(false,"DryId".KVP(DryId)).AsList<OrderVM>();
            return View(model);

        }

        public ActionResult Create()
        {
            SettingVM Obj = new SettingVM();

            return View(Obj);
        }
        [HttpPost]
        public JsonResult Create(OrderFM model)
        {

            
            var data = "SP_AddOrder".ExecuParamsSqlOrStored(false, "DryId".KVP(model.DryId), "OrderCase".KVP(model.OrderCase),
                "OrderDate".KVP(model.OrderDate), "StageId".KVP(model.StageId), "TotalPrice".KVP(model.TotalPrice), "UserId".KVP(model.UserId),"Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                return Json("success," + "تم الحفظ", JsonRequestBehavior.AllowGet);


            }
            else

                return Json("error," + "لم يتم الحفظ", JsonRequestBehavior.AllowGet);


        }


        public ActionResult Edit(int id = 0)
        {

            var StageLits = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            StageLits.AddRange("Sp_STageDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.StageList = StageLits;

            var model = "SP_SelectOrderById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<OrderFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }




    }
}