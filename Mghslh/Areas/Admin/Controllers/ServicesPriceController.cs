﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Areas.Admin.Controllers
{
    public class ServicesPriceController : Controller
    {
        // GET: Admin/ServicesPrice
        public ActionResult Index1()
        {
            

            return View();
        }

        public ActionResult Search()
        {

            int DryId = Convert.ToInt32(Request.Cookies["DrryId"].Value);
            var model = "SP_GetAllServicesPrice".ExecuParamsSqlOrStored(false, "DryId".KVP(DryId)).AsList<ServicesPriceVM>();
            return View(model);

        }


        public ActionResult Create()
        {

            ServicesPriceFM Obj = new ServicesPriceFM();
            var Servicetype = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Servicetype.AddRange("Sp_ServicesTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ServicetypeList = Servicetype;

            var Service = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Service.AddRange("Sp_ServicesDrop".ExecuParamsSqlOrStored(false, "ServiceTypeId".KVP(0)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ServiceList = Service;


            return View(Obj);
        }
        [HttpPost]
        public JsonResult Create(ServicesPriceFM model)
        {
            int DryId = Convert.ToInt32(Request.Cookies["DrryId"].Value);

                       var data = "SP_AddPriceServices".ExecuParamsSqlOrStored(false, "ServicesId".KVP(model.ServiceId),
                "ServicesTypeId".KVP(model.ServiceType), "Price".KVP(model.Price), "DryId".KVP(DryId),"Id".KVP(model.Id)).AsNonQuery();

            if (data != 0)
            {


                return Json("success," + "تم الحفظ", JsonRequestBehavior.AllowGet);


            }
            else


                return Json("error," + "لم يتم الحفظ", JsonRequestBehavior.AllowGet);

        }

        public ActionResult GetServicesList(int id)
        {

            var Service = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Service.AddRange("Sp_ServicesDrop".ExecuParamsSqlOrStored(false,"ServiceTypeId".KVP(id)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ServiceList = Service;

            return Json(Service, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Edit(int id = 0)
        {

            var Servicetype = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Servicetype.AddRange("Sp_ServicesTypeDrop".ExecuParamsSqlOrStored(false).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ServicetypeList = Servicetype;

            var Service = new List<SelectListItem>() { new SelectListItem { Text = "--اختر--", Value = "0" } };
            Service.AddRange("Sp_ServicesDrop".ExecuParamsSqlOrStored(false, "ServiceTypeId".KVP(0)).AsList<SettingDrop>().Select(s => new SelectListItem
            {
                Text = s.Name,
                Value = s.Id.ToString(),
                Selected = s.Id == 0 ? true : false
            }).ToList());
            ViewBag.ServiceList = Service;
            var model = "SP_SelectServicesPriceById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<ServicesPriceFM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            ServicesPriceVM obj = new ServicesPriceVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/ServicesPrice/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteServicesPrice".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}