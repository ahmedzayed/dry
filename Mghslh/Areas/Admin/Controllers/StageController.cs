﻿using Mghslh.Core;
using Mghslh.ViewModel.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Mghslh.Areas.Admin
{
    [Authorize]

    public class StageController : Controller
    {
        // GET: Admin/Stage
        public ActionResult Index1()
        {




            return View();
        }

        public ActionResult Search()
        {
          
                var model = "SP_GetAllStage".ExecuParamsSqlOrStored(false).AsList<StageVM>();
                return View(model);
           
        }


        public ActionResult Create()
        {
            StageVM Obj = new StageVM();

            return View(Obj);
        }
        [HttpPost]
        public JsonResult Create(StageVM model)
        {

            var data = "SP_Addstage".ExecuParamsSqlOrStored(false, "Name".KVP(model.Name),  "Id".KVP(model.Id)).AsNonQuery();
            if (data != 0)
            {


                return Json("success," + "تم الحفظ", JsonRequestBehavior.AllowGet);


            }
            else

            return Json("error," + "لم يتم الحفظ", JsonRequestBehavior.AllowGet);

        }


        public ActionResult Edit(int id = 0)
        {

            var model = "SP_SelectStageById".ExecuParamsSqlOrStored(false, "Id".KVP(id)).AsList<StageVM>();
            if (model == null)
            {
                return HttpNotFound();
            }
            return View("Create", model.FirstOrDefault());
        }



        [HttpGet]
        public ActionResult Delete(int id)
        {
            StageVM obj = new StageVM();
            obj.Id = id;
            return PartialView("~/Areas/Admin/Views/Stage/Delete.cshtml", obj);
        }


        [HttpPost]

        public JsonResult DeleteRow(int Id)
        {
            var data = "SP_DeleteStage".ExecuParamsSqlOrStored(false, "Id".KVP(Id)).AsNonQuery();
            if (data != 0)
            {
                return Json("success," + "تم الحذف بنجاح", JsonRequestBehavior.AllowGet);
            }
            else
                return Json("error," + "لم يتم الحذف", JsonRequestBehavior.AllowGet);
        }

    }
}