﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Mghslh.Startup))]
namespace Mghslh
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
